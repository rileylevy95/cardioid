(ns webjunk.svg
  (:require
   [webjunk.complex :refer [ensure-complex]]
   [webjunk.pseudotag :refer [deftagfn deftag ptag-apply let-dissoc]])
  #?(:cljs (:require-macros [webjunk.pseudotag :refer [deftagfn deftag let-dissoc]])))


(deftagfn line attr [start end & children]
  (let [[x1 y1] (ensure-complex start)
        [x2 y2] (ensure-complex end)]
    [:line (assoc attr :x1 x1 :y1 y1 :x2 x2 :y2 y2)
     children]))


(deftagfn circle attr [center radius & children]
  (let [[cx cy] (ensure-complex center)]
   [:circle (assoc attr :cx cx :cy cy :r radius)
    children]))

(deftagfn rect attr [z dz & children]
  (let [[x     y     ] (ensure-complex z)
        [width height] (ensure-complex dz)]
   [:rect (assoc attr :x x :y y :width width :height height)
    children]))

(deftagfn textlike attr [& body]
  (let-dissoc [{:keys [tag z dz]} attr]
    (let [[x  y ] (ensure-complex z)
          [dx dy] (ensure-complex dz)]
     (ptag-apply tag
                 (merge attr
                        (if  z { :x  x  :y  y})
                        (if dz {:dx dx :dy dy}))
                 body))))

(deftag text  {:tag :text}  textlike)
(deftag tspan {:tag :tspan} textlike)

(deftagfn foreignObject attr [& children]
  (let-dissoc [{:keys [z dz]} attr]
    (let [[x     y     ] (ensure-complex z)
          [width height] (ensure-complex dz)]
      (into [:foreignObject
             (merge attr
                    (if  z {:x     x     :y      y})
                    (if dz {:width width :height height}))]
            children))))
