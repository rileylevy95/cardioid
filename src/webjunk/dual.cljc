(ns webjunk.dual
  (:refer-clojure :exclude [+ - * / inc dec])
  (:require [clojure.pprint :as pprint :refer [cl-format]]
            [clojure.test :as test :refer [is deftest testing]]
            [webjunk.generic-math :refer
             [root-type + - * /
              abs exp conjugate sin cos log sgn pow sqr sqrt tan
              asin acos atan sinh cosh tanh asinh acosh atanh]]

            [webjunk.complex :refer [complex complex? cis i ensure-complex
                                     real imag
                                     #?(:cljs Complex)]])
  #?(:clj (:import [clojure.lang Indexed IFn]
                   [webjunk.complex Complex])))


(declare ensure-dual)

(deftype Dual [point tangent]
  #?(:clj Object :cljs IEquiv)
  (#?(:clj equals :cljs -equiv) [_ y]
    (let [y (ensure-dual y)]
      (= point (.-point y))
      (= tangent (.-tangent y))))
  #?(:clj Indexed :cljs IIndexed)
  (#?(:clj nth :cljs -nth) [_ n default]
    (case n
      0 point
      1 tangent
      default))
  (#?(:clj nth :cljs -nth) [this n]
    (or (nth this n nil)
        (throw (ex-info
                "Dual numbers can only be indexed by 0 or 1." {}
                :index-out-of-bounds))))

  #?@(:cljs
      [IPrintWithWriter
       (-pr-writer [this writer opts]
                   (write-all writer (str point) "+" (str tangent) "ε"))]))
#?(:cljs (derive Dual root-type))

(defn dual
  ([]    (Dual. 0 0))
  ([x]   (Dual. x 0))
  ([x y] (Dual. x y)))

(defn strictly-dual? [z] (= (type z) Dual))
(defn ensure-dual [z]
  (if (strictly-dual? z) z
      (dual z)))

(defmethod ensure-complex Dual [[x t]]
  (dual (ensure-complex x)
        (ensure-complex t)))
(defmethod real Dual [d]
  (let [[[x _] [t _]] (ensure-complex d)]
    (dual x t)))
(defmethod imag Dual [d]
  (let [[[_ y] [_ t]] (ensure-complex d)]
    (dual y t)))

(defmethod complex? Dual [[x t]]
  (and (complex? x) (complex? t)))

(def ε (dual 0 1))
(defn point   [[x _]] x)
(defn tangent [[_ t]] t)

#?(:clj (defmethod print-method Dual [[x y] w]
   (cl-format w "(~a) + (~a)ε" x y)))


(defmethod + [Dual Dual]      [[x t] [y s]] (dual (+ x y) (+ t s)))
(defmethod + [Dual root-type] [ x     y   ] (+ x (ensure-dual y)))
(defmethod + [root-type Dual] [ x     y   ] (+ y x))

(defmethod - Dual [[x t]] (dual (- x) (- t)))

(defmethod * [Dual Dual]      [[x t] [y s]] (dual (* x y)
                                                  (+ (* x s) (* y t))))
(defmethod * [Dual root-type] [ x     y   ] (* x (ensure-dual y)))
(defmethod * [root-type Dual] [ x     y   ] (* y x))

;;;   1
;;; ----- = 1 + x + x² + x³ + ...
;;;  1-x
;;;
;;;   1
;;; ----- = 1 + (-x) + (-x)² + (-x)³ + ...
;;;  1+x
;;;
;;; ε² = 0
;;;
;;;   1
;;; ------ = 1 -tε + (tε)² +... = 1 - tε
;;; 1 + tε
;;;
;;;   1         1/a              1  (       b     )
;;; ------ =  -------------- =  --- (  1 - --- ε  ) = 1/a - b/a² ε
;;; a + bε     1 + (b/a)ε        a  (       a     )
(defmethod / Dual [[a b]] (dual (/ a) (- (/ b (sqr a)))))

(defmethod sgn Dual [[x _]] (dual (sgn x) (if (zero? x) ##Inf 0)))
(defmethod abs Dual [[x t]] (dual (abs x) (* (sgn x) t)))
(defmethod exp Dual [[x t]] (let [y (exp x)]
                              (dual y (* y t))))
;;; log = ∫⅟x dx
;;; log(1+tε) = t - (tε)²/2 + ... = t
;;; log(x + tε) = log(x) + log(1 + t/x ε) = log x + t/x ε
(defmethod log Dual [[x t]] (dual (log x) (/ x t)))

(defmethod sin Dual [[x t]] (dual (sin x) (* t (cos x))))
(defmethod cos Dual [[x t]] (dual (cos x) (* t (- (sin x)))))
(defmethod tan Dual [v] (/ (sin v) (cos v)))

(defmethod conjugate Dual [[x t]]
  (dual (conjugate x) (conjugate t)))
